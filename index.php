<?php
require 'config.php';

ob_start();
try {
    $pageId = array_key_exists('p', $_GET) ? $_GET['p'] : null;

    $sql = "
        SELECT 
            pageID, title, customURL, slug
        FROM
            tblPages
    ";

    $db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_SCHEMA . ';port=' . DB_PORT, DB_USER, DB_PASS);
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $stmt->closeCursor();
    $stmt = null;

    $pages = array();

    foreach ($rows as $row) {
        $pages[$row['pageID']] = array(
            'id' => $row['pageID'],
            'title' => $row['title'],
            'customUrl' => $row['customURL'],
			'slug' => $row['slug']
        );
    }
	
	if ($pageId == null) {
		$slug = isset($_GET['slug']) ? strtolower($_GET['slug']).'' : '';
		foreach ($pages as $page) {
			if ($page['slug'] == $slug) {
				$pageId = $page['id'];
				break;
			}
		}
	} else if ($pageId != null) {
		if (array_key_exists($pageId, $pages)) {
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: /". $pages[$pageId]['slug'] . ($pages[$pageId]['slug'] != '' ? '/' : ''));
			exit();
		} else {
			$pageId = null;
		}
	}

	if ($pageId == null) {
		header('HTTP/1.0 404 Not Found');
        echo '404 : Page Not Found';
        exit();
	} else if ($pages[$pageId]['customUrl']) {
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: " . $pages[$pageId]['customUrl']);
		exit();
	}

    $sql = "
        SELECT 
            p.id, p.content, p.title, p.subtitle, p.photosrc, p.photoAlt,
            l.href, l.text
        FROM 
            tblPosts p
            LEFT JOIN tblLinks l ON p.id = l.postId
        WHERE
            p.pageId = :pageId
    ";

    $stmt = $db->prepare($sql);
    $stmt->execute(array(
        'pageId' => $pageId,
    ));

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $stmt->closeCursor();
    $stmt = null;

    $posts = array();
    foreach ($rows as $row) {
        if (!array_key_exists($row['id'], $posts)) {
            $posts[$row['id']] = array (
                'title' => $row['title'],
                'subtitle' => $row['subtitle'],
                'content' => $row['content'],
                'image' => $row['photosrc'] ? array(
                    'src' => $row['photosrc'],
                    'alt' => $row['photoAlt'],
                ) : null,
                'links' => array(),
            );
        }

        if ($row['href'] != '') {
            $posts[$row['id']]['links'][] = array(
                'href' => $row['href'],
                'text' => $row['text'] != '' ? $row['text'] : $row['href'],
            );
        }
    }

    $db = null;

    function replaceLines($str) {
    	if (strlen($str) > 1) {
    		 $str = nl2br($str);
    	}
    	return $str;
    }

    ?>
    <!DOCTYPE html>

    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Portfolio - <?php echo $pages[$pageId]['title']; ?></title>
        <link rel="stylesheet" href="/style.css" />
        <link rel="icon" href="/img/icon.png" type="image/x-icon">

		<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Person",
			  "email": "mailto:devin@spikedhand.com",
			  "image": "/img/devin-outside.jpg",
			  "jobTitle": "Web Developer",
			  "name": "Devin Handspiker-Wade",
			  "url": "http://spikedhand.com",
			  "sameAs" : [
				"http://www.twitter.com/handspiker2",
				"http://linkedin.com/pub/devin-handspiker-wade/90/73/418"
			  ] 
			}
		</script>
		
        <?php
            if (!IS_DEBUG && defined('GOOGLE_ANALYTICS_CODE')) {
                ?>
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                  ga('create', '<?php echo GOOGLE_ANALYTICS_CODE; ?>', 'auto');
                  ga('send', 'pageview');

                </script>
                <?php
            }
        ?>
    </head>
    <body>
        <header id="pageHeader">
    	    <a id="homepageLink" href="/"><img id="top" src="/img/logoTitle.png" alt="Spiked Hand Development" width="444" height="76"/></a>
    	    <ul id="navList">
    	    <?php
    	    //Menu
    	    foreach ($pages as $page) {
                if ($page['customUrl'] == null) {
                    $url = '/' . $page['slug'] . ($page['slug'] != '' ? '/' : '');
                } else {
                    $url = $page['customUrl'];
                }

                ?>
                <li class="nav">
                    <a href="<?php echo $url; ?>" <?php echo $page['id'] == $pageId ? 'id="current"' :''; ?>><?php echo $page['title']; ?></a>
                </li>
                <?php
    	    }
    	    ?>
        </ul>
        </header>

        <div class="insideBorder">
            <?php
            foreach ($posts as $post) {
            	?>
                <div class="post">
                    <span class="postTitle bold"><?php echo $post['title']?></span>
                    <span class="postSubtitle"><?php echo $post['subtitle']?></span>
                    <?php
                    if ($post['image'] != null) {
                    	?>
                    	<img src="/<?php echo $post['image']['src']?>" alt="<?php echo $post['image']['alt']?>" title="<?php echo $post['image']['alt']?>" />
                    	<?php
                    }
                    ?>

                    <div><?php echo $post['content']; ?></div>
                    <ul>
                        <?php
                        foreach ($post['links'] as $link) {
                            ?>
                            <li>
                                <a href="<?php echo $link['href']; ?>"><?php echo htmlentities($link['text']); ?></a>
                            </li>
                           <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php
            }
            ?>
        </div>

    	<footer class="pageFooter">
    	    <a class="topLink" href="#top">Back to Top</a>
    	</footer>
    </body>
    </html>

    <?php
    echo ob_get_clean();

} catch (Exception $ex) {
    ob_end_flush();

    header('HTTP/1.0 500 Internal Server Error');

    if (IS_DEBUG) {
        echo $ex->getMessage();
    } else {
        echo 'An error has occured';
    }

    error_log($ex);
}

?>

