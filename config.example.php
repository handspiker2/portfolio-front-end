<?php
/*
Make a copy of this file then change the values to match the server environment

Uncomment to set
*/

//define('DB_HOST', 'localhost');
//define('DB_PORT', 3306);
//define('DB_SCHEMA', 'portfolio');
//define('DB_USER', 'user');
//define('DB_PASS', 'password12345$');

//define('GOOGLE_ANALYTICS_CODE', '');

//**********************************************************************
//*****************************STOP**********************************
//**********************************************************************

if (!defined('DB_HOST')) define('DB_HOST', 'localhost');
if (!defined('DB_PORT')) define('DB_PORT', 3306);
